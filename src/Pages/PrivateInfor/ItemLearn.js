import React from "react";
import styles from "./ItemLearn.module.scss";

function ItemLearn({ icon, title, time }) {
  return (
    <div className={`${styles["timeLearn__item"]}`}>
      <i className={`${icon} mr-2`}></i>
      <div>
        <h6>{title}</h6>
        <p>{time}</p>
      </div>
    </div>
  );
}

export default ItemLearn;

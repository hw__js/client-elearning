import React from "react";
import { Tabs } from "antd";
import styles from "./PrivateInfor.module.scss";
import InforTab from "./InforTab";
import InforCourses from "./InforCourses";
import { useSelector } from "react-redux";

const onChange = (key) => {};

function PrivateInfor() {
  const user = useSelector((state) => {
    return state.userReducer.user;
  });

  const renderInfor = () => {
    return <InforTab />;
  };
  const renderCourses = () => {
    return <InforCourses />;
  };

  const items = [
    {
      key: "1",
      label: `Information`,
      children: <>{renderInfor()}</>,
    },
    {
      key: "2",
      label: `Courses`,
      children: <>{renderCourses()}</>,
    },
  ];

  return (
    <div className={`${styles["privateInfor"]} lg:px-20 md:px-12 sm:px-4`}>
      <div className="container-fluid ">
        <div className={`${styles["privateInfor__title"]}`}>
          <h2>Profile & Settings</h2>
        </div>
        <div className={`${styles["privateInfor__content"]}`}>
          <div className="row">
            <div
              className={`${styles["privateInfor__content__left"]} col-lg-3 col-md-4`}
            >
              <img
                src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
                alt="inforImg"
              />
              <h6>{user.hoTen}</h6>
              <p>Front-end Developer</p>
              <button
                className={`${styles["privateInfor__content__left__btn"]}`}
              >
                Personal Information
              </button>
            </div>
            <div
              className={`${styles["privateInfor__content__right"]} col-lg-9 col-md-8`}
            >
              <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PrivateInfor;

import React, { useState } from "react";
import { AudioOutlined } from "@ant-design/icons";
import { Input, Space } from "antd";
import styles from "./InforCourses.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import {
  faCalendar,
  faChartSimple,
  faSearch,
  faSpinner,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { setDeleteCoursesActionService } from "../../Redux/action/CoursesAction";

const { Search } = Input;
const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: "#1890ff",
    }}
  />
);
function InforCourses() {
  const [values, setValues] = useState("");
  const [listCoursesFilter, setListCoursesFilter] = useState([]);
  const coursesListItem = useSelector((state) => {
    return state.coursesReducer.listCoursesRegister;
  });
  const user = useSelector((state) => {
    return state.userReducer.user;
  });

  const dispatch = useDispatch();
  const handleDeleteCourses = (item) => {
    dispatch(
      setDeleteCoursesActionService({
        maKhoaHoc: item.maKhoaHoc,
        taiKhoan: user.taiKhoan,
      })
    );
  };
  const handleChange = (e) => {
    setValues(e.target.value);
    const filterCourses = coursesListItem?.filter((item) => {
      return item.tenKhoaHoc.toLowerCase().includes(values.toLowerCase());
    });
    return setListCoursesFilter(filterCourses);
  };

  const handleRenderItemCourses = () => {
    return coursesListItem?.map((item, index) => {
      return (
        <div
          key={index}
          className="lg:flex items-center justify-between leading-8 border p-2 my-2"
        >
          <div>
            <img
              className="w-full h-full object-cover"
              src={item.hinhAnh}
              alt={item.tenKhoaHoc}
            />
          </div>
          <div className="leading-8 grow px-3 ">
            <div className="flex-col flex justify-center py-2 lg:py-0 lg:text-left md:text-center sm:text-center">
              <h2 className="mt-0 lg:mt-2 text-xl font-bold">
                CoursesName : {item.tenKhoaHoc}
              </h2>
              <h3 className="mt-0 lg:mt-2 text-sm md:text-sm lg:text-base">
                Start : {item.ngayTao}
              </h3>
              <p className="mt-0 lg:mt-2 text-sm md:text-sm lg:text-base">
                Description : {item.moTa}
              </p>
            </div>
            <div className="flex mt-0 lg:mt-2 items-center sm:justify-center md:justify-center lg:justify-start py-2 lg:py-0 lg:text-left md:text-center sm:text-center ">
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon className="text-lg mr-2" icon={faClock} />
                <h2>10 Hours </h2>
              </div>
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon className="text-lg mr-2" icon={faCalendar} />
                <h3>56 minutes</h3>
              </div>
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon
                  className="text-lg mr-2"
                  icon={faChartSimple}
                />
                <h3>All level</h3>
              </div>
            </div>
            <div className="lg:text-left md:text-center sm:text-center">
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
            </div>
            <div className="mt-3 flex sm:justify-center md:justify-center lg:justify-start  lg:text-left md:text-center sm:text-center">
              <img
                className="w-8 h-8 rounded-full block ring-2 ring-offset-4 mr-3"
                src="https://source.unsplash.com/40x40/?portrait?1"
                alt=""
              />
              <span>
                Lecturer :
                {item.nguoiTao.hoTen.length > 0
                  ? item.nguoiTao.hoTen
                  : "Ẩn danh"}
              </span>
            </div>
          </div>
          <div className="sm:text-center md:text-center lg:text-left">
            <button
              onClick={() => {
                handleDeleteCourses(item);
              }}
              className="lg:px-3 lg:py-2 md:px-4 md:py-2 sm:px-4 sm:py-2 sm:mt-3 md:mt-3 lg:mt-0 bg-teal-500 text-white lg:text-lg md:text-base text-base outline-transparent focus:outline-transparent hover:opacity-80 rounded-sm min-w-150"
            >
              Cancel Course
            </button>
          </div>
        </div>
      );
    });
  };
  const handleRenderSearchListCourses = () => {
    return listCoursesFilter.map((item, index) => {
      return (
        <div
          key={index}
          className="lg:flex items-center justify-around leading-8 border p-2 my-2"
        >
          <div className="">
            <img
              className=" w-full h-full block object-cover"
              src={item.hinhAnh}
              alt={item.tenKhoaHoc}
            />
          </div>
          <div
            className="leading-8 grow px-3"
            style={{
              paddingBottom: "24px !important ",
            }}
          >
            <div className="flex-col flex justify-center py-2 lg:py-0 lg:text-left md:text-center sm:text-center">
              <h2 className="mt-0 lg:mt-2 text-xl font-bold">
                Course Name : {item.tenKhoaHoc}
              </h2>
              <h3 className="mt-0 lg:mt-2 text-sm md:text-sm lg:text-base">
                Start : {item.ngayTao}
              </h3>
              <p className="mt-0 lg:mt-2 text-sm md:text-sm lg:text-base">
                Description : {item.moTa}
              </p>
            </div>
            <div className="flex mt-0 lg:mt-2 items-center sm:justify-center md:justify-center lg:justify-start py-2 lg:py-0 lg:text-left md:text-center sm:text-center ">
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon className="text-lg mr-2" icon={faClock} />
                <h2>10 Hours </h2>
              </div>
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon className="text-lg mr-2" icon={faCalendar} />
                <h3>56 minutes</h3>
              </div>
              <div className="flex mt-0 lg:mt-2 py-2 mr-2">
                <FontAwesomeIcon
                  className="text-lg mr-2"
                  icon={faChartSimple}
                />
                <h3>All level</h3>
              </div>
            </div>
            <div className="lg:text-left md:text-center sm:text-center">
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
              <FontAwesomeIcon
                className="text-xl text-yellow-500 mr-2"
                icon={faStar}
              />
            </div>
            <div className="mt-3 flex sm:justify-center md:justify-center lg:justify-start  lg:text-left md:text-center sm:text-center">
              <img
                className="w-6 h-6 rounded-full ring-2 ring-offset-4 mr-3"
                src="https://source.unsplash.com/40x40/?portrait?1"
                alt=""
              />
              <span>
                Lecturer :
                {item.nguoiTao.hoTen.length > 0
                  ? item.nguoiTao.hoTen
                  : "Ẩn danh"}
              </span>
            </div>
          </div>
          <button
            onClick={() => {
              handleDeleteCourses(item);
            }}
            className="px-3 py-2 mt-6 md:mt-0 lg:mt-0 bg-teal-500 h-full text-white lg:text-lg md:text-base sm:text-base text-base outline-transparent focus:outline-transparent hover:opacity-80 rounded-sm min-w-150"
          >
            Cancel Course
          </button>
        </div>
      );
    });
  };
  return (
    <div className={`${styles["inforCourses"]}`}>
      <section className={`${styles["myCourses"]}`}>
        <div
          className={`${styles["myCourses__findCourses"]} md:flex lg:flex justify-between text-center md:text-left lg:text-left items-center`}
        >
          <h6 className="mb-3 md:mb-0 lg:mb-0 text-6xl">My Courses</h6>
          <div className="border  flex items-center justify-between rounded-sm bg-slate-150 h-10">
            <input
              type="text"
              onChange={handleChange}
              placeholder="Search Courses"
              className="outline-none h-full  ml-2 bg-slate-150"
            />
            {values.length > 0 ? (
              <button className=" h-full  px-3 outline-none bg-slate-150 focus:outline-none text-teal-500 hover:opacity-80 hover:bg-slate-100 border-l-2">
                <FontAwesomeIcon
                  className="block animate-spin"
                  icon={faSpinner}
                />
              </button>
            ) : (
              <button className=" h-full  px-3 outline-none bg-slate-150 focus:outline-none text-teal-500 hover:opacity-80 hover:bg-slate-100 border-l-2">
                <FontAwesomeIcon className="block" icon={faSearch} />
              </button>
            )}
          </div>
        </div>
        <div className="mt-5">
          {values.length > 0
            ? handleRenderSearchListCourses()
            : handleRenderItemCourses()}
        </div>
      </section>
    </div>
  );
}

export default InforCourses;

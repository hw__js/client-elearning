import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "./InforTab.module.scss";
import ItemLearn from "./ItemLearn";
import * as Yup from "yup";
import { useFormik } from "formik";
import { getUserInfor, userUpdate } from "../../Redux/action/UserAction";
import { useNavigate } from "react-router-dom";

function InforTab() {
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user, userInfor } = useSelector((state) => state.userReducer);

  let regexAccount =
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

  let regexPassword =
    /^[a-zA-Z_ÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" + "ẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" + "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\\s]+$/;

  let regexPhoneNumber = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;

  let { email, hoTen, maNhom, taiKhoan, maLoaiNguoiDung } = userInfor;

  // Formik form
  const formik = useFormik({
    initialValues: {
      taiKhoan: user.taiKhoan,
      matKhau: "",
      hoTen: "",
      email: "",
      soDT: "",
      maLoaiNguoiDung: user.maLoaiNguoiDung,
      maNhom: "GP01",
    },
    validationSchema: Yup.object().shape({
      matKhau: Yup.string()
        .required("Account must not be empty")
        .matches(
          regexAccount,
          "Password must be at least 8 characters including letters, numbers, and special characters"
        ),

      hoTen: Yup.string()
        .required("Name must not be blank")
        .matches(regexPassword, "Enter only alphanumeric characters"),

      email: Yup.string()
        .email("Invalid Email")
        .required("Email must not be blank"),

      soDT: Yup.string()
        .required("Phone number must not be left blank")
        .matches(regexPhoneNumber, "Invalid phone number"),
    }),
    onSubmit: (values, formik) => {
      dispatch(userUpdate(values, formik));
    },
  });

  useEffect(() => {
    dispatch(getUserInfor);
  }, []);

  return (
    <div className={`${styles["inforPersonal"]}`}>
      <section className={`${styles["userInfor"]}`}>
        <div className={`${styles["userInfor__top"]} row`}>
          <div className="col-md-7">
            <p>
              Email:<span className="ml-2">{email}</span>
            </p>
            <p>
              Full name: <span className="ml-2">{hoTen}</span>
            </p>
            <p>
              Phone number:
              <span className="ml-2">
                {userInfor.soDt ? userInfor.soDt : userInfor.soDT}
              </span>
            </p>
          </div>
          <div className="col-md-5">
            <p>
              Account: <span className="ml-2">{taiKhoan}</span>
            </p>
            <p>
              Group: <span className="ml-2">{maNhom}</span>
            </p>
            <p>
              Object:
              <span className="ml-2">
                {maLoaiNguoiDung === "HV" ? " Student" : " Teacher"}
              </span>
            </p>

            <button
              onClick={() => {
                setShow(!show);
              }}
              className={`${styles["btnGlobal"]}`}
            >
              Update
            </button>
          </div>
        </div>

        {/* User Tab Bottom  */}
        <div className={`${styles["userInfor__bottom"]}`}>
          <h4>My Skills</h4>
          <div className="row">
            <div className={`${styles["mySkills"]} col-xl-8 col-lg-6 pb-5`}>
              <div
                className={`${styles["mySkills__html"]} ${styles["pskill"]}`}
              >
                <button className={`${styles["mySkills__Custom"]}`}>
                  HTML
                </button>
                <div className={`${styles["mySkills__progress"]} `}>
                  <div
                    className={`${styles["mySkills__progress__bar"]}`}
                    aria-valuemin={0}
                    aria-valuemax={100}
                    aria-valuenow={75}
                  ></div>
                </div>
              </div>

              <div className={`${styles["mySkills__css"]} ${styles["pskill"]}`}>
                <button className={`${styles["mySkills__Custom"]}`}>CSS</button>
                <div className={`${styles["mySkills__progress"]}`}>
                  <div className={`${styles["mySkills__progress__bar"]}`}></div>
                </div>
              </div>

              <div className={`${styles["mySkills__js"]} ${styles["pskill"]}`}>
                <button className={`${styles["mySkills__Custom"]}`}>JS</button>
                <div className={`${styles["mySkills__progress"]}`}>
                  <div className={`${styles["mySkills__progress__bar"]}`}></div>
                </div>
              </div>

              <div
                className={`${styles["mySkills__react"]} ${styles["pskill"]}`}
              >
                <button className={`${styles["mySkills__Custom"]}`}>
                  React
                </button>
                <div className={`${styles["mySkills__progress"]}`}>
                  <div className={`${styles["mySkills__progress__bar"]}`}></div>
                </div>
              </div>
            </div>
            <div className={`${styles["timeLearn"]} col-xl-4 col-lg-6`}>
              <ItemLearn icon={"fas fa-user-clock"} title={"Hours"} time={80} />
              <ItemLearn
                icon={"fas fa-layer-group"}
                title={"Lesson"}
                time={80}
              />
              <ItemLearn icon={"fas fa-swatchbook"} title={"Total"} time={40} />
              <ItemLearn
                icon={"fas fa-signal"}
                title={"Level"}
                time={"University"}
              />
              <ItemLearn
                icon={"fas fa-graduation-cap"}
                title={"Capacity"}
                time={"Good"}
              />
              <ItemLearn icon={"fas fa-book"} title={"Homework"} time={100} />
            </div>
          </div>
        </div>

        {/* Modal  */}
        {show ? (
          <div
            className={`${styles["modalForm"]}`}
            style={{ paddingLeft: "0" }}
          >
            {/* modal-dialog */}
            <div className={`${styles["modalForm__wrapper"]}`}>
              <div className={`${styles["modal-content"]}`}>
                <div className={`modal-header ${styles["modalUpdateHeader"]}`}>
                  <h5 className={`${styles["modal-title"]}`}>
                    Update Personal Information
                  </h5>

                  <button
                    type="button"
                    className="close"
                    onClick={() => {
                      setShow(!show);
                    }}
                  >
                    ×
                  </button>
                </div>
                <div className={`modal-body ${styles["modalUpdate"]}`}>
                  <form onSubmit={formik.handleSubmit}>
                    <h6>Full Name</h6>
                    <input
                      type="text"
                      placeholder="Full Name"
                      name="hoTen"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.hoTen}
                    />
                    {formik.errors.hoTen && formik.touched.hoTen ? (
                      <div className={`${styles["errorMessage"]}`}>
                        {formik.errors.hoTen}
                      </div>
                    ) : (
                      <div className={`${styles["message"]}`}></div>
                    )}

                    <h6>Password</h6>
                    <input
                      type="password"
                      placeholder="Password"
                      name="matKhau"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.matKhau}
                    />
                    {formik.errors.matKhau && formik.touched.matKhau ? (
                      <div className={`${styles["errorMessage"]}`}>
                        {formik.errors.matKhau}
                      </div>
                    ) : (
                      <div className={`${styles["message"]}`}></div>
                    )}

                    <h6>Email</h6>
                    <input
                      type="email"
                      placeholder="Email"
                      name="email"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.email}
                    />
                    {formik.errors.email && formik.touched.email ? (
                      <div className={`${styles["errorMessage"]}`}>
                        {formik.errors.email}
                      </div>
                    ) : (
                      <div className={`${styles["message"]}`}></div>
                    )}

                    <h6>Phone Number</h6>
                    <input
                      type="phone"
                      placeholder="Phone number"
                      name="soDT"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.soDT}
                    />
                    {formik.errors.soDT && formik.touched.soDT ? (
                      <div className={`${styles["errorMessage"]}`}>
                        {formik.errors.soDT}
                      </div>
                    ) : (
                      <div className={`${styles["message"]}`}></div>
                    )}

                    {/* Button Form  */}
                    <div className={`${styles["form-footer"]} modal-footer`}>
                      <button
                        type="submit"
                        className={`${styles["btnSubmit"]}`}
                      >
                        Complete
                      </button>
                      <button
                        type="button"
                        className={`${styles["btnSubmit"]} ${styles["btnClose"]}`}
                        onClick={() => {
                          setShow(!show);
                        }}
                      >
                        Close
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </section>
    </div>
  );
}

export default InforTab;

import React from "react";
import { Desktop, Mobile, Tablet } from "./../../HOC/responsive";
import CoursesListDeskTopAndMobile from "./CourListDeskTopAndMobile";
import CoursesListTablet from "./CoursesListTablet";

export default function () {
  return (
    <>
      <Desktop>
        <CoursesListDeskTopAndMobile />
      </Desktop>
      <Tablet>
        <CoursesListTablet />
      </Tablet>
      <Mobile>
        <CoursesListDeskTopAndMobile />
      </Mobile>
    </>
  );
}

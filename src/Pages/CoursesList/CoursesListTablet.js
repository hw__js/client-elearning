import React, { useEffect, useState } from "react";
import { getCourseListPagination } from "./../../Service/CoursesService";
import { Pagination } from "antd";
import styles from "./CoursesList.module.scss";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

export default function CoursesListTablet() {
  const [listCourses, setListCourses] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const user = useSelector((state) => {
    return state.userReducer.user;
  });
  console.log(user);
  const onChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    getCourseListPagination(currentPage)
      .then((res) => {
        setListCourses(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [currentPage]);
  const handleRenderListCourses = () => {
    return listCourses?.items.map((item, index) => {
      console.log(item);
      return (
        <>
          <div
            key={index}
            className={`${styles["courseList__itemCourse"]} shadow-2xl rounded-md`}
          >
            <div
              className={`${styles["courseList__itemCourse__img"]}`}
              style={{ height: 220, borderRadius: "6px" }}
            >
              <img
                // src={item.hinhAnh}
                className="h-60 object-cover"
                src={item?.hinhAnh}
                alt="img"
              />
            </div>
            <div
              className={`${styles["courseList__itemCourse__content"]} text-center pb-3`}
            >
              <h3>{item.tenKhoaHoc}</h3>
              <ul
                className={`${styles["courseList__itemCourse__content__admin"]} flex items-center justify-center`}
              >
                <li>
                  <img
                    src="https://source.unsplash.com/40x40/?portrait?1"
                    className="rounded-circle"
                    alt="Admin"
                    style={{
                      height: 25,
                      width: 25,
                      borderRadius: "50%",
                      overflow: "hidden",
                    }}
                  />
                </li>
                <li>
                  <span>By</span>
                </li>
                <li>{item.nguoiTao.hoTen}</li>
              </ul>
              <h4 className="text-base">
                <del>$84.99</del> $9.99
              </h4>
            </div>
            <div className={`${styles["courseList__itemCourse__hover"]}`}>
              <div
                className={`${styles["courseList__itemCourse__hover__container"]}`}
              >
                <div>
                  <h3>
                    <a href="/">
                      Linux Administration Bootcamp: Go from Beginner to
                      Advanced
                    </a>
                  </h3>
                  <p>
                    Hello. My name is Admin Cannon and I'm the author of Linux
                    for Beginners, the founder of the Linux Training
                  </p>
                  <div
                    className={`${styles["courseList__itemCourse__hover__container__btn"]}`}
                  >
                    <NavLink
                      to={`/detail/${item.maKhoaHoc}`}
                      className="block no-underline hover:no-underline hover:text-white"
                    >
                      <button className={`${styles["default__btn"]}`}>
                        Detail
                      </button>
                    </NavLink>
                    {user ? (
                      <NavLink
                        to={`/enroll-in/${item.maKhoaHoc}`}
                        className="block no-underline hover:no-underline hover:text-white"
                      >
                        <button className={`${styles["default__btn"]}`}>
                          Join in
                        </button>
                      </NavLink>
                    ) : (
                      <NavLink
                        to={`/login`}
                        className="block no-underline hover:no-underline hover:text-white"
                      >
                        <button className={`${styles["default__btn"]}`}>
                          Join in
                        </button>
                      </NavLink>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    });
  };
  return (
    <div className="bg-gray-50">
      <div className="py-10 px-8">
        <div className="grid lg:grid-cols-5 md:grid-cols-3 grid-cols-1 gap-3 ">
          {handleRenderListCourses()}
        </div>
        <div className="mt-4 text-center">
          <Pagination
            defaultCurrent={currentPage}
            total={listCourses?.totalCount}
            onChange={onChange}
          />
        </div>
      </div>
    </div>
  );
}

import React from "react";
import styles from "./NumberYears.module.scss";
import AVTR1 from "../../../assets/img/j37ln6cye17aqjfgnisb.png";
import AVTR2 from "../../../assets/img/foaoty9yj8ztpyn3rygg.png";
import AVTR3 from "../../../assets/img/fw2vmnixtr8rwhb8jga8.png";
import AVTR4 from "../../../assets/img/cmejwacwcz2ejxhv8wna.png";
import AVTR5 from "../../../assets/img/sbwdmgrtzl0ttu3ntwt1.png";
import AVTR6 from "../../../assets/img/fdsav9xalgcj6mjqs1rn.png";
import AVTR7 from "../../../assets/img/xfdt2sondensxindk4xf.png";

// import Swiper core and required modules
import { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

import a from "./NumberYears.module.scss";

const data = [
  {
    avatar: AVTR1,
  },
  {
    avatar: AVTR2,
  },
  {
    avatar: AVTR3,
  },
  {
    avatar: AVTR4,
  },
  {
    avatar: AVTR5,
  },
  {
    avatar: AVTR6,
  },
  {
    avatar: AVTR7,
  },
];

function NumberYears() {
  return (
    // <div className={`${styles["numberYears"]}`}>
    //   <div className="container"></div>
    // </div>
    <section id={`${a["numberYears"]}`}>
      <Swiper
        className={`container ${a["numberYears__container"]} `}
        // install Swiper modules
        modules={[Pagination]}
        spaceBetween={40}
        slidesPerView={6}
        navigation
        // pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={() => {}}
        onSlideChange={() => {}}
      >
        {data.map(({ avatar }, index) => {
          return (
            <SwiperSlide key={index} className={`${a["numberYears"]}`}>
              <div
                className={`${a["numberYears__avatar"]}`}
                style={{ opacity: 1, transform: "none" }}
              >
                <img src={avatar} alt="img" />
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
}

export default NumberYears;

import React from "react";
import { NavLink } from "react-router-dom";
import ItemCourse from "../PopularCourses/ItemCourse";
import img from "../../../assets/img/courses-shape.png";
import styles from "./ViewedCourses.module.scss";

function ViewedCourses({ coursesArr }) {
  const renderViewedCourses = () => {
    return coursesArr.slice(0, 4).map((item, index) => {
      return <ItemCourse item={item} key={index} />;
    });
  };

  return (
    <div className={`${styles["viewedCourses"]}`}>
      <div className="container-fluid">
        <div className={`${styles["viewedCourses__title"]}`}>
          <div
            className={`${styles["viewedCourses__title__left"]}`}
            style={{ opacity: 1, transform: "none" }}
          >
            <div className={`${styles["viewedCourses__title__left__text"]}`}>
              <span className={`${styles["top-title"]}`}>
                Most Viewed Courses
              </span>
              <h2>Students Are Also Viewing</h2>
            </div>
          </div>
          <NavLink
            className={`${styles["viewedCourses__title__right"]}`}
            to="/courses-list"
          >
            View All
          </NavLink>
        </div>
        <div className="row justify-center">{renderViewedCourses()}</div>
      </div>
      <img className={`${styles["courses-shape"]}`} src={img} alt="img" />
    </div>
  );
}

export default ViewedCourses;

import React from "react";
import testimonial_img from "../../../assets/img/testimonial-1.png";
import styles from "./SayAboutUs.module.scss";

function SayAboutUs() {
  return (
    <div className={`${styles["testimonial"]}`}>
      <div className="container">
        <div className="row items-center">
          <div className="col-lg-6">
            <div className={`${styles["testimonial__img"]}`}>
              <img src={testimonial_img} alt="testimonial" />
            </div>
          </div>
          <div className="col-lg-6">
            <div className={`${styles["testimonial__content"]}`}>
              <h2>Our Students Are Our Strength. See What They Say About Us</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SayAboutUs;

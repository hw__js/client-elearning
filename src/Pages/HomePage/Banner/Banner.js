import React from "react";
import bannerImg from "../../../assets/img/banner-img-1.png";
import client1 from "../../../assets/img/client-1.jpg";
import client2 from "../../../assets/img/client-2.jpg";
import client3 from "../../../assets/img/client-3.jpg";
import shape1 from "../../../assets/img/shape-1.svg";
import shape2 from "../../../assets/img/shape-2.svg";
import shape3 from "../../../assets/img/shape-3.svg";
import styles from "./Banner.module.scss";

function Banner() {
  return (
    <div className={`${styles["homePage__banner"]}`}>
      <div className={`${styles["container-fluid"]}`}>
        <div className="row items-center">
          {/* LEFT-CORNER  */}
          <div className="col-lg-6">
            <div className={`${styles["homePage__banner__img"]}`}>
              <img src={bannerImg} alt="banner" style={{ transform: "none" }} />
            </div>
          </div>

          {/* RIGHT-CORNER  */}
          <div className="col-lg-6">
            <div className={`${styles["homePage__banner__content"]}`}>
              <h1>Improve Your Online Learning Experience Better Instantly </h1>

              <p style={{ opacity: 1, transform: "none" }}>
                We have <span>40k+</span> Online courses &amp;
                <span>500K+</span> Online registered student. Find your desired
                Courses from them.
              </p>

              <form className={`${styles["search-form"]}`}>
                <input
                  type="text"
                  className={`${styles["form-control"]}`}
                  placeholder="Search Courses"
                  name="search"
                />
                <button type="submit" className={`${styles["default-btn"]}`}>
                  <span>Search Now</span>
                  <i className="fa fa-search" />
                </button>
              </form>

              <ul className={`${styles["client-list"]} `}>
                <li>
                  <img src={client1} alt="banner" />
                  <img
                    src={client2}
                    className={`${styles["client"]}`}
                    alt="banner"
                  />
                  <img
                    src={client3}
                    className={`${styles["client"]}`}
                    alt="banner"
                  />
                </li>
                <li>
                  <p>
                    500K+ People already trusted us.
                    <a className={`${styles["read-more"]}`} href="/">
                      View Courses<i className="fa fa-arrow-right"></i>
                    </a>
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <img
        src={shape1}
        className={`${styles["shape"]} ${styles["shape-1"]}`}
        alt="banner"
      />
      <img
        src={shape2}
        className={`${styles["shape"]} ${styles["shape-2"]}`}
        alt="banner"
      />
      <img
        src={shape3}
        className={`${styles["shape"]} ${styles["shape-3"]}`}
        alt="banner"
      />
    </div>
  );
}

export default Banner;

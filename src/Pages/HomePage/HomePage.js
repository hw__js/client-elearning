import React, { useEffect, useState } from "react";
import { getCoursesList } from "../../Service/CoursesService";
import Banner from "./Banner/Banner";
import PopularCourses from "./PopularCourses/PopularCourses";
import ViewedCourses from "./ViewedCourses/ViewedCourses";
import OurFeatures from "./OurFeatures/OurFeatures";
import SayAboutUs from "./SayAboutUs/SayAboutUs";
import NumberYears from "./NumberYears/NumberYears";

function HomePage() {
  const [coursesArr, setCoursesArr] = useState([]);

  useEffect(() => {
    getCoursesList()
      .then((res) => {
        setCoursesArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Banner />
      <PopularCourses coursesArr={coursesArr} />
      <ViewedCourses coursesArr={coursesArr} />
      <OurFeatures />
      <SayAboutUs />
      <NumberYears />
    </div>
  );
}

export default HomePage;

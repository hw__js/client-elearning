import React from "react";
import styles from "./ItemFeature.module.scss";

function ItemFeature({ image, title }) {
  return (
    <div className="col-lg-3 col-sm-6">
      <div className={`${styles["itemFeature"]}`}>
        <img src={image} alt="" />
        <h3>{title}</h3>
        <p>
          Instructors from around the world teach millions of students on Edmy
          through video.
        </p>
      </div>
    </div>
  );
}

export default ItemFeature;

import React from "react";
import ItemFeature from "./ItemFeature";
import featureImg1 from "../../../assets/img/feature-1.svg";
import featureImg2 from "../../../assets/img/feature-2.svg";
import featureImg3 from "../../../assets/img/feature-3.svg";
import featureImg4 from "../../../assets/img/feature-4.svg";
import imgShape from "../../../assets/img/feature-shape-1.svg";
import styles from "./OurFeatures.module.scss";

function OurFeatures() {
  return (
    <div className={`${styles["ourFeatures"]}`}>
      <div className={`${styles["container"]}`}>
        <div className={`${styles["ourFeatures__title"]}`}>
          <span className={`${styles["top-title"]}`}>Our Features</span>
          <h2>Why You Should Choose Edmy</h2>
        </div>
        <div className={`${styles["ourFeatures__content"]} row justify-center`}>
          <ItemFeature image={featureImg1} title={"Expert-Led Video Courses"} />
          <ItemFeature image={featureImg2} title={"In-Demand Trendy Topics"} />
          <ItemFeature image={featureImg3} title={"Segment Your Learning"} />
          <ItemFeature
            image={featureImg4}
            title={"Always Interactive Learning"}
          />
        </div>
      </div>
      <img className={`${styles["shape"]}`} src={imgShape} alt="img" />
    </div>
  );
}

export default OurFeatures;

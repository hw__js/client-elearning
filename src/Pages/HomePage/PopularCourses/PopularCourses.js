import React from "react";
import { useNavigate } from "react-router-dom";
import ItemCourse from "./ItemCourse";
import styles from "./PopularCourses.module.scss";

function PopularCourses({ coursesArr }) {
  let navigate = useNavigate();

  const handleNavigate = (id) => {
    setTimeout(() => {
      navigate(`/detail/${id}`);
    }, 1000);
  };
  const renderPopularCourses = () => {
    return coursesArr.slice(0, 4).map((item, index) => {
      return <ItemCourse item={item} key={index} />;
    });
  };

  return (
    <div className={`${styles["popularCourses"]}`}>
      <div className={`${styles["container"]}`}>
        <div
          className={`${styles["popularCourses__title"]}`}
          style={{ opacity: 1, transform: "none" }}
        >
          <span>Popular Courses</span>
          <h2>Expand Your Career Opportunity With Our Courses</h2>
        </div>

        <div className="row justify-center">{renderPopularCourses()}</div>
      </div>
    </div>
  );
}

export default PopularCourses;

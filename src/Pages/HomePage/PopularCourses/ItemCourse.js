import React from "react";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import styles from "./ItemCourse.module.scss";
import { NavLink } from "react-router-dom";

function ItemCourse({ item }) {
  //
  return (
    <div className="col-lg-3 col-md-6">
      <div className={`${styles["itemCourse"]} shadow-2xl rounded-md`}>
        <div
          className={`${styles["itemCourse__img"]}`}
          style={{ height: 220, borderRadius: "6px" }}
        >
          <img
            // src={item.hinhAnh}
            className="h-60 object-cover"
            src="https://res.cloudinary.com/dev-empty/image/upload/v1660634163/icvgops1gqcosgv3dxde.jpg"
            alt="img"
          />
        </div>
        <div className={`${styles["itemCourse__content"]} text-center pb-3`}>
          <h3>
            {item.tenKhoaHoc.length > 30
              ? item.tenKhoaHoc.slice(0, 30) + "..."
              : item.tenKhoaHoc}
          </h3>
          <ul
            className={`${styles["itemCourse__content__admin"]} flex items-center justify-center`}
          >
            <li>
              <img
                src="https://source.unsplash.com/40x40/?portrait?1"
                className="rounded-circle"
                alt="Admin"
                style={{
                  height: 25,
                  width: 25,
                  borderRadius: "50%",
                  overflow: "hidden",
                }}
              />
            </li>
            <li>
              <span>By</span>
            </li>
            <li>{item.nguoiTao.hoTen}</li>
          </ul>
          <h4 className="text-base">
            <del>$84.99</del> $9.99
          </h4>
        </div>
        <div className={`${styles["itemCourse__hover"]}`}>
          <div className={`${styles["itemCourse__hover__container"]}`}>
            <div>
              <h3>
                <a href="/">
                  Linux Administration Bootcamp: Go from Beginner to Advanced
                </a>
              </h3>
              <p>
                Hello. My name is Admin Cannon and I'm the author of Linux for
                Beginners, the founder of the Linux Training
              </p>
              <div className={`${styles["itemCourse__hover__container__btn"]}`}>
                <button className={`${styles["default__btn"]}`}>
                  <NavLink
                    to={`/detail/${item.maKhoaHoc}`}
                    className="block no-underline hover:no-underline hover:text-white"
                  >
                    Detail
                  </NavLink>
                </button>
                <button className={`${styles["default__btn"]}`}>
                  <NavLink
                    to={`/enroll-in/${item.maKhoaHoc}`}
                    className="block no-underline hover:no-underline hover:text-white"
                  >
                    Join in
                  </NavLink>
                </button>
                <button
                  className={`${styles["default__btn"]} ${styles["like"]} hidden`}
                >
                  <FavoriteBorderIcon className={`${styles["heart"]}`} />
                  <FavoriteIcon
                    className={`${styles["heart"]} ${styles["hover"]}`}
                  />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemCourse;

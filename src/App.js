import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage.js";
import HomePage from "./Pages/HomePage/HomePage";
import LayoutDefault from "./HOC/LayoutDefault";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import DetailPage from "./Pages/DetaiPage/DetailPage";
import CoursesList from "./Pages/CoursesList/CoursesList";
import PrivateInfor from "./Pages/PrivateInfor/PrivateInfor";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <LayoutDefault>
              <HomePage />
            </LayoutDefault>
          }
        />
        <Route
          path="/login"
          element={
            <LayoutDefault>
              <LoginPage />
            </LayoutDefault>
          }
        />
        <Route
          path="/register"
          element={
            <LayoutDefault>
              <RegisterPage />
            </LayoutDefault>
          }
        />
        <Route
          path="detail/:id"
          element={
            <LayoutDefault>
              <DetailPage />
            </LayoutDefault>
          }
        />
        <Route
          path="/courses-list"
          element={
            <LayoutDefault>
              <CoursesList />
            </LayoutDefault>
          }
        />
        <Route
          path="/infor"
          element={
            <LayoutDefault>
              <PrivateInfor />
            </LayoutDefault>
          }
        />
        <Route
          path="*"
          element={
            <LayoutDefault>
              <NotFoundPage />
            </LayoutDefault>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

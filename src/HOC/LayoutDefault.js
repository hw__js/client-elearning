import React from "react";
import Footer from "../Component/Footer/Footer";
import Header from "../Component/Header/Header";
import MoveToTop from "../Component/MoveToTop/MoveToTop";

export default function LayoutDefault({ children }) {
  return (
    <div>
      <Header />
      {children}
      <Footer />
      <MoveToTop />
    </div>
  );
}

import React from "react";
import BackToTop from "react-back-to-top-button";
import styles from "./MoveToTop.module.scss";

function MoveToTop() {
  return (
    <>
      <BackToTop
        showOnScrollUp
        showAt={10}
        speed={10000}
        easing="easeInOutQuint"
      >
        <a href="#" className={`${styles["move"]}`}>
          <div className={`${styles["move__icon"]}`}>
            <i className="fa fa-arrow-up"></i>
          </div>
        </a>
      </BackToTop>
    </>
  );
}

export default MoveToTop;

import React from "react";
import { Desktop, Mobile, Tablet } from "./../../../HOC/responsive";
import Top_HeaderDesktop from "./Top_HeaderDesktop";
import Top_HeaderMobile from "./Top_HeaderMobile";
import Top_HeaderTablet from "./Top_HeaderTablet";

export default function Top_Header() {
  return (
    <div>
      <Desktop>
        <Top_HeaderDesktop />
      </Desktop>
      <Tablet>
        <Top_HeaderTablet />
      </Tablet>
      <Mobile>
        <Top_HeaderMobile />
      </Mobile>
    </div>
  );
}

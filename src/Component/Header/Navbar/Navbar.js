import React from "react";
import { Desktop, Mobile, Tablet } from "./../../../HOC/responsive";
import NavbarDeskTopAndTablet from "./NavbarDeskTopAndTablet";
import NavbarMobile from "./NavbarMobile";

export default function Navbar() {
  return (
    <>
      <Desktop>
        <NavbarDeskTopAndTablet />
      </Desktop>
      <Tablet>
        <NavbarDeskTopAndTablet />
      </Tablet>
      <Mobile>
        <NavbarMobile />
      </Mobile>
    </>
  );
}

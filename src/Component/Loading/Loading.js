import React from "react";
import { useSelector } from "react-redux";

export default function Loading() {
  const { loadingComponent } = useSelector((state) => state.IsLoadingReducer);
  // console.log(loadingComponent);
  if (loadingComponent) {
    return (
      <>
        <div id="preloader">
          <div id="loader"></div>
        </div>
      </>
    );
  } else {
    return "";
  }
}

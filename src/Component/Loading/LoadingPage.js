import React from "react";
import { useSelector } from "react-redux";

export default function PageLoading() {
  const { loadingGlobal } = useSelector((state) => state.IsLoadingReducer);
  if (loadingGlobal) {
    return (
      <>
        <div className="loader">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </>
    );
  } else {
    return "";
  }
}

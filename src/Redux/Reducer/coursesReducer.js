import { coursesListLocalService } from "../../Service/LocalService";
import {
  SET_DELETE_ITEM_COURSES,
  SET_REGISTER_LIST_COURSES,
} from "../Constant/CoursesConstant";

const initialState = {
  listCoursesRegister: coursesListLocalService.get(),
};

export const coursesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_REGISTER_LIST_COURSES:
      {
        let newListCoursesRegister = [...state.listCoursesRegister];
        let index = newListCoursesRegister.findIndex((item) => {
          return item.maKhoaHoc === payload.maKhoaHoc;
        });
        if (index === -1) {
          let itemCourses = { ...payload };
          newListCoursesRegister.push(itemCourses);
          coursesListLocalService.set(newListCoursesRegister);
          state.listCoursesRegister = newListCoursesRegister;
        }
      }
      return { ...state, ...payload };

    case SET_DELETE_ITEM_COURSES:
      {
        let newListCoursesDelete = [...state.listCoursesRegister];
        let index = newListCoursesDelete.findIndex((item) => {
          return item.maKhoaHoc === payload.maKhoaHoc;
        });
        if (index !== -1) {
          newListCoursesDelete.splice(index, 1);
          coursesListLocalService.set(newListCoursesDelete);
          state.listCoursesRegister = newListCoursesDelete;
        }
      }
      return { ...state, ...payload };

    default:
      return state;
  }
};

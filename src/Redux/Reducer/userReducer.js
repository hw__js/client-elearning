import { userLocalService } from "../../Service/LocalService";
import {
  SET_USER_INFOR,
  SET_USER_LOGIN,
  SET_USER_LOG_OUT,
  SET_USER_REGISTER,
  UPDATE_USER_INFOR,
} from "../Constant/UserConstants";

const initialState = {
  user: userLocalService.get(),
  userInfor: userLocalService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN:
      return { ...state, user: payload };
    case SET_USER_REGISTER:
      return { ...state };
    case SET_USER_LOG_OUT:
      return { ...state, user: payload };
    case UPDATE_USER_INFOR:
      return { ...state, userInfor: payload };
    case SET_USER_INFOR:
      return { ...state, userInfor: payload };
    default:
      return state;
  }
};

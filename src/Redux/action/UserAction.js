import {
  SET_USER_INFOR,
  SET_USER_LOGIN,
  SET_USER_LOG_OUT,
  UPDATE_USER_INFOR,
} from "../Constant/UserConstants";
import { message } from "antd";
import {
  postLogin,
  postUserInfor,
  putUserInfor,
} from "../../Service/UserService";
import {
  coursesListLocalService,
  userLocalService,
} from "../../Service/LocalService";
import swal from "sweetalert";

export const setUserAction = (values) => {
  return {
    type: SET_USER_LOGIN,
    payload: values,
  };
};
export const setUserLoginActionService = (values, handleSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        swal({
          title: "Successfully Login",
          icon: "success",
          timer: 2000,
          button: false,
        });
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data,
        });
        userLocalService.set(res.data);
        handleSuccess();
      })
      .catch((err) => {
        swal({
          title: err.response.data,
          icon: "warning",
          text: "An error occurred, please return to the homepage or try again",
          timer: 2000,
          button: false,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      });
  };
};

export const setUserLogOutActionService = (values) => {
  return (dispatch) => {
    dispatch({
      type: SET_USER_LOG_OUT,
      payload: values,
    });
    userLocalService.remove();
    coursesListLocalService.remove();
  };
};

// Update user information
export const userUpdate = (values, formik) => {
  return (dispatch) => {
    putUserInfor(values)
      .then((res) => {
        swal({
          title: "Successfully Updated",
          icon: "success",
          timer: 2000,
          button: false,
        });
        dispatch({
          type: UPDATE_USER_INFOR,
          payload: res.data,
        });
        formik.resetForm();
        userLocalService.set(res.data);
      })
      .catch((err) => {
        swal({
          title: err.response?.data,
          icon: "warning",
          text: "An error occurred, please try again",
          timer: 2000,
          button: false,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      });
  };
};
// hoang phu
// Anh&123456
// Get user information
export const getUserInfor = (values) => {
  return (dispatch) => {
    postUserInfor(values)
      .then((res) => {
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data,
        });
      })
      .catch((err) => {
        message.error(
          `${err.response.data} Please come back homepage or try again `
        );
      });
  };
};

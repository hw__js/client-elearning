import { message } from "antd";
import {
  postCancelCourses,
  postRegisterCourses,
} from "../../Service/CoursesService";
import {
  SET_DELETE_ITEM_COURSES,
  SET_REGISTER_LIST_COURSES,
} from "../Constant/CoursesConstant";

import swal from "sweetalert";

export const setRegisterCourseActionService = (values, detail) => {
  return (dispatch) => {
    postRegisterCourses(values)
      .then((res) => {
        swal({
          title: "Successfully Registered",
          icon: "success",
          timer: 2000,
          button: false,
        });
        dispatch({
          type: SET_REGISTER_LIST_COURSES,
          payload: detail,
        });
      })
      .catch((err) => {
        swal({
          title: err.response?.data,
          icon: "warning",
          text: "An error occurred, please return to the homepage or try again",
          timer: 2000,
          button: false,
        });
      });
  };
};
export const setDeleteCoursesActionService = (values) => {
  return (dispatch) => {
    postCancelCourses(values)
      .then((res) => {
        swal({
          title: "Successfully Deleted",
          icon: "success",
          timer: 2000,
          button: false,
        });
        dispatch({
          type: SET_DELETE_ITEM_COURSES,
          payload: values,
        });
      })
      .catch((err) => {
        swal({
          title: err.response?.data,
          icon: "warning",
          text: "An error occurred, please return to the homepage or try again",
          timer: 2000,
          button: false,
        });
      });
  };
};

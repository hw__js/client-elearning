import { https } from "./ConfigURL";

export const getCoursesList = () => {
  return https.get(`/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`);
};
export const getDetailCourses = (id) => {
  return https.get(`/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`);
};

export const getCourseListPagination = (currentPage) => {
  return https.get(
    `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?page=${currentPage}&pageSize=10&MaNhom=GP01`
  );
};
export const postRegisterCourses = (data) => {
  return https.post("/api/QuanLyKhoaHoc/DangKyKhoaHoc", data);
};
export const postCancelCourses = (data) => {
  return https.post("/api/QuanLyKhoaHoc/HuyGhiDanh", data);
};

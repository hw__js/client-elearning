import { https } from "./ConfigURL";

export let postLogin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};
export let postRegister = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangKy", data);
};
export let postUserInfor = (data) => {
  return https.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan", data);
};
export let putUserInfor = (data) => {
  return https.put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data);
};

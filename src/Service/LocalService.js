export const USER_LOCAL = "USER_LOCAL";
export const COURSES_LIST = "COURSES_LIST";

export const userLocalService = {
  get: () => {
    let userJson = localStorage.getItem(USER_LOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (userData) => {
    let userJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOCAL, userJson);
  },
  remove: () => {
    localStorage.removeItem(USER_LOCAL);
  },
};

export const coursesListLocalService = {
  get: () => {
    let coursesJson = localStorage.getItem(COURSES_LIST);
    if (coursesJson) {
      return JSON.parse(coursesJson);
    } else {
      return [];
    }
  },
  set: (courses) => {
    let coursesJson = JSON.stringify(courses);
    localStorage.setItem(COURSES_LIST, coursesJson);
  },
  remove: () => {
    localStorage.removeItem(COURSES_LIST);
  },
};
